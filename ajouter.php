<!doctype html>
  <html>
    <head>
      <title>Vidéothèque</title>
      <link rel='stylesheet' href='film.css'>
    </head>
    <body>
        <header>
            <h1> Vidéothèque </h1>
            <ul id="sous_menu">
            <li><a href="../Projet_PHP/main_view.php"> Nos films</a></li>
            <li>Gestion films</li>
            <li><a href="../Projet_PHP/ajouter.php"> Ajouter films</a></li>
            <li><a href="../Projet_PHP/supprimer.php"> Supprimer films</a></li>
            <li><a href="../Projet_PHP/contact.php"> Contacts</a></li>
            </ul>
        </header>
        <section>
            <?php
            $file_db = new PDO("sqlite:Films.sqlite");
            $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

            ?>
            <form action="ajouter.php" method="post">
                <p>Titre original du film: <input type="text" name="TitreVO"></p>
                <p>Titre Francais du film: <input type="text" name="TitreVF"></p>
                <p><label>Pays d'origine du film: </label>
                    <select name="Pays" id="pays">
                        <option value="">Faites votre choix</option>
                        <?php
                            $result= $file_db->query("SELECT distinct pays FROM films order by pays");
                            foreach($result as $elem){
                                echo "<option value='$elem[0]'>$elem[0]</option>";
                            }
                        ?>
                    </select>
                </p>
                <p><label>Année de sortie du film: </label>
                    <select name="Annee" id="annee">
                        <option value="">Faites votre choix</option>
                        <?php
                        $date=date("Y");
                        for($i=1900;$i<=(int)$date;$i++){
                            echo "<option value='$i'>$i</option>";
                        }
                        ?>
                    </select>
                </p>
                <p><label>Couleur du film: </label>
                <select name="Couleur" id="couleur">
                    <option value="">Faites votre choix</option>
                    <option value="couleur">Couleur</option>
                    <option value="nb">NB</option>
                </select>
                </p>
                <p>Durée du film (en minute): <input type="text" name="Duree"></p>
                <p><label>Réalisateur du film: </label>
                    <select name="Realisateur" id="realisateur">
                        <option value="">Faites votre choix</option>
                        <?php
                            $result= $file_db->query("SELECT distinct nom,prenom,code_indiv from individus natural join films where realisateur = code_indiv order by nom");
                            foreach($result as $elem){
                                echo "<option value='$elem[2]'>$elem[0]$elem[1]</option>";
                            }

                        ?>
                    </select>
                </p>
                <input type="submit" value="Ajouter">
            </form>


            <?php
            try{
                if($_SERVER["REQUEST_METHOD"]!="GET"){

                    $insert = "INSERT INTO films(code_film,titre_original,titre_francais,pays,date,duree,couleur,realisateur,image) VALUES (:code_film,:titre_original,:titre_francais,:pays,:date,:duree,:couleur,:realisateur,:image)";
                    $stmt = $file_db->prepare($insert);

                    $code=0;
                    $titreVO="";
                    $titreVF="";
                    $pays="";
                    $date=0;
                    $duree=0;
                    $couleur="";
                    $realisateur=0;
                    $image="";

                    $stmt->bindParam(":code_film", $code);
                    $stmt->bindParam(":titre_original", $titreVO);
                    $stmt->bindParam(":titre_francais", $titreVF);
                    $stmt->bindParam(":pays", $pays);
                    $stmt->bindParam(":date", $date);
                    $stmt->bindParam(":duree", $duree);
                    $stmt->bindParam(":couleur", $couleur);
                    $stmt->bindParam(":realisateur", $realisateur);
                    $stmt->bindParam(":image", $image);

                    $result=$file_db->query("SELECT max(code_film) FROM films");
                    foreach($result as $elem){
                        $code=$elem[0]+1;
                    }
                    $titreVO = $_POST["TitreVO"];
                    $titreVF = $_POST["TitreVF"];
                    $pays = $_POST["Pays"];
                    $date = $_POST["Annee"];
                    $duree = $_POST["Duree"];
                    $couleur = $_POST["Couleur"];
                    $realisateur = $_POST["Realisateur"];
                    $image = $_POST["TitreVO"].".jpg";
                    if($titreVO!=""){
                        if($titreVF!=""){
                            if($pays!=""){
                                if($date!=""){
                                    if($couleur!=""){
                                        if($duree!=""){
                                            if($realisateur!=""){
                                                $stmt->execute();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch(PDOException $e){
                echo $e->getMessage();
            }
            $file_db=null;
            ?>

        </section>
    </body>
</html>

​
