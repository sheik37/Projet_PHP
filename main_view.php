<!doctype html>
  <html>
  <link rel='stylesheet' href='film_view.css'>
    <head>
      <title>Vidéothèque</title>
      <link rel="stylesheet" href="logo_test.jpg" type="icon" />
    </head>
    <body>
        <header>
          <h1>Vidéothèque</h1>
          <!<img src="logo_test.jpg" alt="Logo" >
          <a href="main.php"> Retour</a>
        </header>
        <section>
          <?php
          $file_db = new PDO("sqlite:Films.sqlite"); # ouverture de la bd
          $result= $file_db->query("SELECT titre_francais FROM films order by titre_francais"); #query de l'affichage basique
          ?>
          <form method="post" action="main_view.php"> <!Ajout d'un form pour les recherches>
            <nav>
              <section id=genres>
                <label>Genres : </label> <!Liste déroulante des genres>
                    <select name="Genres" id="genres">
                      <option value=" "> </option>
                      <?php
                      $result = $file_db->query("SELECT nom_genre from genres order by nom_genre");
                        foreach($result as $c){
                          echo "<option value='$c[nom_genre]'>$c[nom_genre]</option>";
                        }
                      ?>
                    </select>
              </section>
              <section id=annees>
                <label>Année de réalisssée de ation : </label> <!Liste déroulante des années>
                    <select name="Annee" id="annee">
                      <option value=" "> </option>
                      <?php
                      $result = $file_db->query("SELECT distinct date from films order by date");
                        foreach($result as $c){
                          echo "<option value='$c[date]'>$c[date]</option>";
                        }
                      ?>
                    </select>
              </section>
              <section id=reali>
                <label>Réalisateur : </label> <!Liste déroulante des réalisateurs>
                  <select name="Reali" id="real">
                    <option value=" "> </option>
                    <?php
                    $result = $file_db->query("SELECT distinct nom,prenom from individus natural join films where realisateur = code_indiv order by nom");
                      foreach($result as $c){
                        echo "<option value='$c[nom]'>$c[nom] $c[prenom]</option>";
                      }
                    ?>
                  </select>
              </section>
              <section id =bouton>
                <input type="submit" value="Rechercher"> <! Bouton de recherche>
              </section>
              <section id =search>
                <input type="text" name="search" autofocus=""> <! Bar de recherche>
              </section>
            </nav>
            <?php
            if($_SERVER["REQUEST_METHOD"]!="GET"){
              if($_POST["Genres"] != " " and $_POST["Annee"] == " " and $_POST['Reali']== " "){ #recherche pour les genres
                $result= $file_db->query("SELECT distinct titre_francais FROM films natural join classification natural join genres where nom_genre like '".$_POST['Genres']."%' and ref_code_film = code_film and code_genre = ref_code_genre");
              }
              else if($_POST["Genres"] == " " and $_POST["Annee"] != " " and $_POST['Reali']== " " ) { #recherche pour les années
                $result= $file_db->query("SELECT titre_francais FROM films where date = '".$_POST['Annee']."'");
              }
              else if($_POST["Genres"]!=" " and $_POST['Annee']!=" " and $_POST['Reali']== " "){ #recherche pour les années et genres
                $result= $file_db->query("SELECT distinct titre_francais FROM films natural join classification natural join genres where nom_genre like '".$_POST['Genres']."%' and ref_code_film = code_film and code_genre = ref_code_genre and date = '".$_POST['Annee']."'");
              }
              else if($_POST["Genres"] == " " and $_POST["Annee"] == " " and $_POST['Reali']!= " "){ #recherche pour les realisateurs
                $result= $file_db->query("SELECT distinct titre_francais FROM films natural join individus where realisateur = code_indiv and nom like '".$_POST['Reali']."%' order by titre_francais");
              }
              else if($_POST["Genres"] == " " and $_POST["Annee"] != " " and $_POST['Reali']!= " "){ #recherche pour les realisateurs et années
                $result= $file_db->query("SELECT distinct titre_francais FROM films natural join individus where realisateur = code_indiv and nom like '".$_POST['Reali']."%' and date = '".$_POST['Annee']."'");
              }
              else if($_POST["Genres"] != " " and $_POST["Annee"] == " " and $_POST['Reali']!= " "){ #recherche pour les realisateurs et genres
                $result= $file_db->query("SELECT distinct titre_francais FROM films natural join classification natural join genres natural join individus where nom_genre like '".$_POST['Genres']."%' and ref_code_film = code_film and code_genre = ref_code_genre and realisateur = code_indiv and nom like '".$_POST['Reali']."%'");
              }
              else if($_POST["Genres"]!=" " and $_POST['Annee']!=" " and $_POST['Reali']!= " "){ #recherche pour les 3
                $result= $file_db->query("SELECT distinct titre_francais FROM films natural join classification natural join genres natural join individus where nom_genre like '".$_POST['Genres']."%' and ref_code_film = code_film and code_genre = ref_code_genre and date = '".$_POST['Annee']."' and realisateur = code_indiv and nom like '".$_POST['Reali']."%'");
              }
              else if($_POST["search"]!=""){ #recherhce dans la barre de recherche
                $result=$file_db->query("SELECT distinct titre_francais FROM films natural join classification natural join genres natural join individus
                  where ref_code_film = code_film and code_genre = ref_code_genre and realisateur = code_indiv
                  and (date like '".$_POST['search']."%'
                  or nom_genre like '".$_POST['search']."%'
                  or nom like '".$_POST['search']."%'
                  or titre_francais like '".$_POST['search']."%'
                  or titre_original like '".$_POST['search']."%')");
              }
              else {
                $result= $file_db->query("SELECT titre_francais FROM films order by titre_francais");
              }
              } else {
                $result= $file_db->query("SELECT titre_francais FROM films order by titre_francais");
              }
              echo "<table>";
              $cpt = 0;
              foreach($result as $c){
                if($cpt%5==0){
                  echo '<tr>';
                }

              echo "<form method='GET' action='test.php'>";
              echo "<td><input type='submit' value ='$c[titre_francais]' name='titre'></td></form>";

              $cpt++;
              if($cpt%5==0){
                echo '</tr>';
              }
              }
              if ($cpt==0 and ($_POST["search"]=="")) {
                echo "<p id=echec>Aucun résultat </p>";
              }
              echo "</table>";
              $file_db = null;
           ?>
          </form>
        </section>
    </body>
</html>

​
